<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
    <title>Document</title>
</head>

<body>
    <header>
        <img class="background" src="<?php echo get_template_directory_uri(); ?>/img/Rectangle_1.png" alt="">
        <div class="top_left_logo_container">
            <img class="lion_logo" src="<?php echo get_template_directory_uri(); ?>/img/logo_UL.png" alt="">
            <img class="insa_logo" src="<?php echo get_template_directory_uri(); ?>/img/logo_INSA.png" alt="">
        </div>
        <div class="account">
            <i class="far fa-user"></i><a href="">My Account</a>
        </div>
        <div class="top_right_logo">
            <a href=""><i class="fab fa-facebook-f"></i></a>
            <a href=""><i class="fab fa-instagram"></i></a>
            <a href=""><i class="fab fa-youtube"></i></a>
            <a href=""><i class="fab fa-linkedin-in"></i></a>
        </div>
        <p class="title">Trust them</p>
        <div class="header_left_poly"></div>
        <div class="header_right_poly"></div>
    </header>
</body>

</html>